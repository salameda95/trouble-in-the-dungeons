using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public void ChangeCursorVisible(bool state)
    {
        Cursor.visible = state;
    }
}
