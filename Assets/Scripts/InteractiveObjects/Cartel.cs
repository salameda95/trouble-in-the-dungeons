using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Cartel : MonoBehaviour
{
    public Dialog dialog;
    public Animator _keyAnimator;
    public GameObject keyIcon;
    public PlayerInput _playerInput;

    void Start()
    {
        keyIcon.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            keyIcon.SetActive(true);
            ChangeInteractIcons();
            //other.SendMessageUpwards("SetDialog", dialog);
            other.GetComponent<PlayerActions>().SetDialog(dialog, keyIcon);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            keyIcon.SetActive(false);
            other.SendMessageUpwards("UnsetDialog");
        }
            
    }

    public void ChangeInteractIcons()
    {
        if (_playerInput.currentControlScheme.ToLower().Contains("keyboard"))
            _keyAnimator.SetBool("usingKeyboard", true);
        else
            _keyAnimator.SetBool("usingKeyboard", false);
    }
}
