using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class IconKeyDialog : MonoBehaviour
{
    public PlayerInput _playerInput;

    Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    void OnEnable()
    {
        ChangeReadIcon();
    }

    public void ChangeReadIcon()
    {
        if (_playerInput.currentControlScheme.ToLower().Contains("keyboard"))
            _animator.SetBool("usingKeyboard", true);
        else
            _animator.SetBool("usingKeyboard", false);
    }
}
