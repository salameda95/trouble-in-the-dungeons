using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class DialogManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;
    public float textSpeed = 0.013f;
    public GameObject dialogBox;
    public PlayerActions _playerActions;

    Queue<string> sentences;
    GameObject cartelKey;
    bool isIntro = false;
    AudioManager _audioManager;

    void Awake()
    {
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartDialog(Dialog dialog, GameObject _object)
    {
        isIntro = false;

        dialogBox.SetActive(true);
        if(_object != null)
        {
            cartelKey = _object;
            cartelKey.SetActive(false);
        }

        nameText.text = dialog.name;

        sentences.Clear();

        foreach (string sentence in dialog.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void StartIntroDialog(Dialog dialog)
    {
        isIntro = true;

        dialogBox.SetActive(true);

        nameText.text = dialog.name;

        sentences.Clear();

        foreach (string sentence in dialog.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        _audioManager.PlaySound("openDialog");
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(textSpeed);
        }
    }

    void EndDialogue()
    {
        _playerActions.isInDialog = false;
        dialogBox.SetActive(false);

        if(cartelKey != null)
        {
            cartelKey.SetActive(true);
            cartelKey.transform.parent.GetComponent<Cartel>().ChangeInteractIcons();
            cartelKey = null;
        }

        if (isIntro)
        {
            _playerActions.canOpenDialog = false;
        }
            

        isIntro = false;
    }
}
