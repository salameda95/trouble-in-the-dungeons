using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerHealth : MonoBehaviour
{
    public int totalHealth = 2;
    public RectTransform heartUI;
    public GameObject gameOverMenu;
    public Transform spawn;
    public GameObject deathPrefab;
    public GameObject retryButton;
    public GameSettings _gameSettings;

    int health;
    float hearthSize = 16f;
    SpriteRenderer _renderer;
    Animator _animator;
    Rigidbody2D rb;
    bool invulnerability = false;
    AudioManager _audioManager;

    private void Awake()
    {
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        _renderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        health = totalHealth;
    }

    public void AddDamage(int amount)
    {
        if (!invulnerability)
        {
            health = health - amount;
            rb.AddForce(Vector2.up * 5f, ForceMode2D.Impulse);
            StartCoroutine("VisualFeedback");
            StartCoroutine("Invulnerability");

            heartUI.sizeDelta = new Vector2(hearthSize * health, hearthSize);
            if (health <= 0)
            {
                _audioManager.PlaySound("death");
                health = 0;
                gameObject.SetActive(false);
                Instantiate(deathPrefab, this.transform.position, Quaternion.identity);
                
                gameOverMenu.SetActive(true);
                GameObject.Find("Cannon").GetComponent<Cannon>().stop = true;
                GetComponent<PlayerActions>().SetGameOver(true);
                EventSystem.current.SetSelectedGameObject(retryButton);
                _gameSettings.ChangeCursorVisible(true);
            }else
                _audioManager.PlaySound("hit");

        }
    }

    public void AddHealth(int amount, GameObject item)
    {
        health = health + amount;

        if (health > totalHealth)
        {
            health = totalHealth;
        }
        else
        {
            if(item != null)
            {
                _audioManager.PlaySound("health");
                item.SetActive(false);
            }
                
        }
        heartUI.sizeDelta = new Vector2(hearthSize * health, hearthSize);
    }

    IEnumerator VisualFeedback()
    {
        _renderer.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        _renderer.color = Color.white;
    }

    IEnumerator Invulnerability()
    {
        invulnerability = true;
        yield return new WaitForSeconds(1f);

        invulnerability = false;
    }

    void OnEnable()
    {
        health = totalHealth;
        heartUI.sizeDelta = new Vector2(hearthSize * health, hearthSize);
        _renderer.color = Color.white;
        invulnerability = false;
        gameObject.transform.position = new Vector3(spawn.transform.position.x, spawn.transform.position.y);
        _gameSettings.ChangeCursorVisible(false);
    }

    void OnDisable()
    {
        _animator.enabled = false;
    }
}
