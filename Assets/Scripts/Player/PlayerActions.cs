using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerActions : MonoBehaviour
{
    public Transform groundCheck; //Componente Transform donde se comprobara si esta sobre en contacto con el suelo.
    public LayerMask groundLayer; //Contiene la Layer Ground.
    public float jumpPower = 2.5f; //Potencia de salto.
    public float speed = 8f; //Velocidad de movimiento del jugador.
    public bool isInDialog = false; //Comprueba si esta en un dialogo, si lo esta no podra usar la mayoria de controles.
    public bool canOpenDialog = false; //Comprueba si el jugador puede abrir un dialogo.
    public Dialog dialog; //Contiene el dialogo que esta cargado para mostrarse en la ventana de dialogo, cuando sea necesario.
    public GameObject cartelKey; //Contiene el Game Object de la tecla de interaccion del cartel actual.
    public GameObject pauseMenu; //Contiene el Game Object del menu de Pausa.
    public GameObject continueButton; //Contiene el Game Object del bot�n de continuar del menu de Pausa.
    public Cartel [] cartels = new Cartel[6]; //Array que contiene todos los carteles de la escena.
    public IconKeyDialog _iconKeyDialog; //Contiene el icono de la tecla de continuar en la iterfaz de dialogos.
    public GameSettings _gameSettings;
    public GameObject[] gameObjectsToRespawn; //Game Object a reaparecer cuando reviva el jugador.
    public AudioSource ambientalAudio; //Se utiliza para controlar el audio ambiente de la escena.

    float horizontal; //Almacena la velocidad horizontal del jugador.
    float vertical; //Almacena la velocidad vertical del jugador.
    bool isFacingRight = true; //Comprueba hacia donde esta mirando.
    bool isAttacking = false; //Comprueba si esta atacando.
    bool isCrawl = false; //Comprueba si esta agachado.
    bool isPaused = false; //Comprueba si esta pausada la partida
    bool isGameOver = false; //Comprueba si esta en Game Over.
    Animator _animator;
    Rigidbody2D rb;
    PlayerInput _playerInput;
    AudioManager _audioManager;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _playerInput = GetComponent<PlayerInput>();
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    void OnEnable()
    {
        Time.timeScale = 1;
    }

    private void Update()
    {
        ChangeIcons();
    }

    void FixedUpdate()
    {

        //Si no ataca y no esta agachado podra moverse, pero si ataca o esta agachado se le impide el movimiento al jugador
        if (!isAttacking && !isCrawl)
            rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
        else if(isAttacking || isCrawl)
            rb.velocity = new Vector2(0f, rb.velocity.y);

        //Voltea al jugador en funciona en funcion de hacia donde este mirando.
        if (!isFacingRight && horizontal > 0f)
            Flip();
        else if (isFacingRight && horizontal < 0f)
            Flip();
    }

    void LateUpdate()
    {
        //Se actualizan las variables del animator.
        _animator.SetBool("Idle", new Vector2(horizontal, 0f) == Vector2.zero);
        _animator.SetBool("IsCrawl", vertical < 0f);
        _animator.SetBool("IsGrounded", IsGrounded());
        _animator.SetFloat("VerticalVelocity", rb.velocity.y);

        if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
            isAttacking = true;
        else
            isAttacking = false;

        if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("Crawl"))
            isCrawl = true;
        else
            isCrawl = false;
    }

    public bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    //Voltea al personaje, en la direccion contraria en la que se encuentre.
    void Flip()
    {
        if (!isPaused && !isGameOver)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }

    public void Move(InputAction.CallbackContext context)
    {
        if (!isPaused && !isGameOver)
        {
            if (!isInDialog)
            {
                horizontal = context.ReadValue<Vector2>().x;
                vertical = context.ReadValue<Vector2>().y;
            }
        }
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if (!isPaused && !isGameOver)
        {
            if (!isInDialog)
            {
                if (context.performed && IsGrounded())
                {
                    rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
                    _audioManager.PlaySound("jump");
                }
                //Si se suelta antes de tiempo el jugador no llegara a la altura maxima.
                if (context.canceled && rb.velocity.y > 0f)
                    rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
            }
            else
            {
                if (context.performed)
                    FindObjectOfType<DialogManager>().DisplayNextSentence();
            }
        }
    }

    public void Attack(InputAction.CallbackContext context)
    {
        if (!isPaused && !isGameOver)
        {
            if (!isInDialog)
            {
                if (context.performed && IsGrounded() && !isAttacking)
                {
                    _animator.SetBool("IsCrawl", false);
                    _animator.SetTrigger("Attack");
                    rb.velocity = Vector2.zero;
                    isAttacking = true;
                    _audioManager.PlaySound("attack");
                }
            }
        }
    }

    public void ReadText(InputAction.CallbackContext context)
    {
        if (!isPaused && !isGameOver)
        {
            if (context.performed && !isInDialog && canOpenDialog)
            {
                isInDialog = true;
                horizontal = 0f;
                FindObjectOfType<DialogManager>().StartDialog(dialog, cartelKey);
                _audioManager.PlaySound("openDialog");
            }
        }
    }

    public void ReadTextTutorial(Dialog newDialog)
    {
        dialog = newDialog;
        canOpenDialog = true;

        isInDialog = true;
        horizontal = 0f;
        FindObjectOfType<DialogManager>().StartIntroDialog(dialog);
    }

    public void ChangeIcons()
    {
        if(_playerInput.enabled == true)
        {
            foreach (var cartel in cartels)
                cartel.ChangeInteractIcons();

            if (isInDialog)
                _iconKeyDialog.ChangeReadIcon();
        }
        
    }

    public void SetDialog(Dialog newDialog, GameObject keyInteraction)
    {
        dialog = newDialog;
        cartelKey = keyInteraction;
        canOpenDialog = true;
    }

    public void UnsetDialog()
    {
        dialog = null;
        cartelKey = null;
        canOpenDialog = false;
    }

    public void PauseGame(InputAction.CallbackContext context)
    {
        if (!isPaused)
        {
            if (!isGameOver && context.performed)
            {
                ambientalAudio.Pause();
                _audioManager.PlaySound("pause");
                pauseMenu.SetActive(true);
                isPaused = true;
                Time.timeScale = 0;
                _gameSettings.ChangeCursorVisible(true);

                EventSystem.current.SetSelectedGameObject(continueButton);
            }
        }
        else if(isPaused)
        {
            if (!isGameOver && context.performed)
            {
                ambientalAudio.Play();
                _audioManager.PlaySound("pause");
                pauseMenu.SetActive(false);
                isPaused = false;
                Time.timeScale = 1;
                _gameSettings.ChangeCursorVisible(false);

                EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }

    public void Resume()
    {
        _audioManager.PlaySound("pause");
        ambientalAudio.Play();
        pauseMenu.SetActive(false);
        isPaused = false;
        Time.timeScale = 1;
        _gameSettings.ChangeCursorVisible(false);

        EventSystem.current.SetSelectedGameObject(null);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void RetryStage()
    {
        gameObject.transform.Find("HitBox").GetComponent<BoxCollider2D>().enabled = false;
        gameObject.SetActive(true);
        isGameOver = false;
        GameObject.Find("Canvas").transform.Find("GameOver").gameObject.SetActive(false);
        GameObject.Find("Cannon").GetComponent<Cannon>().stop = false;
        EventSystem.current.SetSelectedGameObject(null);
        GetComponent<Animator>().enabled = true;
        ambientalAudio.Play();

        foreach (GameObject _gameObject in gameObjectsToRespawn)
            _gameObject.SetActive(false);

        foreach (GameObject _gameObject in gameObjectsToRespawn)
            _gameObject.SetActive(true);
    }

    public void BackMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("PrincipalMenu");
    }

    public void SetGameOver(bool state)
    {
        isGameOver = state;
        if (state)
            ambientalAudio.Stop();
    }

    public void PlaySoundFromAnimation(string sound)
    {
        _audioManager.PlaySound(sound);
    }
}
