using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public GameObject deathPrefab;

    AudioManager _audioManager;

    void Awake()
    {
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            Instantiate(deathPrefab, collision.transform.parent.gameObject.transform.position, Quaternion.identity);
            collision.transform.parent.gameObject.SetActive(false);

            if (collision.transform.parent.gameObject.name.ToLower().Contains("rat"))
                _audioManager.PlaySound("ratDeath");
            else if(collision.transform.parent.gameObject.name.ToLower().Contains("soldier"))
                _audioManager.PlaySound("soldierDeath");
        }
    }
}
