using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rat : MonoBehaviour
{
    public float speed = 8f;
    public LayerMask groundLayer;
    public float originalPosition;
    public bool originalFacingRight;
    public float leftLimit; //Posion x minima a la que puede llegar.
    public float rightLimit; //Posion x maxima a la que puede llegar.

    Vector2 actualPosition; //Almacena la posicion a la que se tendra que dirigir.
    bool arrivedToPos = false; //Comprueba si ha llegado a la posicion.
    bool isFacingRight = false; //Comprueba hacia donde esta mirando.
    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnEnable()
    {
        if (transform.localScale.x < 0f)
            isFacingRight = true;
        else if (transform.localScale.x > 0f)
            isFacingRight = false;

        SetNewPosition();
    }

    void OnDisable()
    {
        transform.position = new Vector2(originalPosition, transform.position.y);
        arrivedToPos = false;

        if (originalFacingRight)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            actualPosition = new Vector2(rightLimit, transform.position.y);
        }
        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            actualPosition = new Vector2(leftLimit, transform.position.y);
        }
    }

    void FixedUpdate()
    {
        rb.MovePosition(Vector2.MoveTowards(rb.position, actualPosition, speed * Time.deltaTime));

        if (!arrivedToPos)
        {
            //Si ha llegado a un muro, se volteara
            if (Physics2D.Raycast(transform.position, actualPosition, 1.5f, groundLayer))
            {
                arrivedToPos = true;
                Flip();
            }
            //Si ha llegado al destino , se volteara.
            if ((Vector2.Distance(transform.position, actualPosition) < 0.3f && !isFacingRight) || Vector2.Distance(transform.position, actualPosition) < 0.3f && isFacingRight)
            {
                arrivedToPos = true;
                Flip();
            }
        }
    }

    //Se voltea.
    void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 localScale = transform.localScale;
        localScale.x *= -1f;
        transform.localScale = localScale;

        //Actualiza la posicion
        SetNewPosition();
        arrivedToPos = false;
    }

    //Actualiza la posicion hacia la que se tiene que dirigir el enemigo.
    void SetNewPosition()
    {
        if (!isFacingRight)
            actualPosition = new Vector2(leftLimit, transform.position.y);
        else
            actualPosition = new Vector2(rightLimit, transform.position.y);
    }
}
