using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : MonoBehaviour
{
    public float speed = 8f;
    public LayerMask groundLayer;
    public float leftLimit; //Posion x minima a la que puede llegar.
    public float rightLimit; //Posion x maxima a la que puede llegar.
    public GameObject playerDetect; //GameObject que tiene el collider para detectar al jugador y atacarlo.

    Animator _animator;
    Vector2 actualPosition; //Almacena la posicion a la que se tendra que dirigir.
    Rigidbody2D rb;
    bool arrivedToPos = false; //Comprueba si ha llegado a la posicion.
    bool isFacingRight = false; //Comprueba hacia donde esta mirando.
    bool isIdle = false; //Comprueba si esta en Idle.
    bool isAttacking = false; //Comprueba si esta atacando.

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    void OnEnable()
    {
        if (transform.localScale.x > 0f)
            isFacingRight = true;
        else if (transform.localScale.x < 0f)
            isFacingRight = false;

        SetNewPosition();
    }

    void OnDisable()
    {
        StopAllCoroutines();
        transform.position = new Vector2(leftLimit+1f, transform.position.y);
        isIdle = false;
        isAttacking = false;
        arrivedToPos = false;
        actualPosition = new Vector2(rightLimit, transform.position.y);
        transform.localScale = new Vector3(1f, 1f, 1f);
        playerDetect.SetActive(true);
    }

    void FixedUpdate()
    {
        //Se podra mover si no esta atacando o si no esta en Idle.
        if (!isIdle && !isAttacking)
        {
            rb.MovePosition(Vector2.MoveTowards(rb.position, actualPosition, speed * Time.deltaTime));

            if (!arrivedToPos)
            {
                //Si ha llegado a un muro, se volteara
                if (Physics2D.Raycast(transform.position, actualPosition, 1.5f, groundLayer))
                {
                    arrivedToPos = true;
                    Flip();
                }
                //Si ha llegado al destino , ejecutara la corutina IdleForASeconds.
                if ((Vector2.Distance(transform.position, actualPosition) < 0.3f && !isFacingRight) || Vector2.Distance(transform.position, actualPosition) < 0.3f && isFacingRight)
                {
                    arrivedToPos = true;
                    StartCoroutine("IdleForASeconds");
                }
            }
        }
    }

    void LateUpdate()
    {
        _animator.SetBool("Idle", isIdle);
    }

    //El enemigo al llegar a su posicion se parara unos segundos.
    IEnumerator IdleForASeconds()
    {
        isIdle = true;

        yield return new WaitForSeconds(3);

        Flip();
    }

    //Voltea al soldado.
    void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 localScale = transform.localScale;
        localScale.x *= -1f;
        transform.localScale = localScale;

        //Detendra la corutina que provocaba que estuviese en idle el soldado.
        StopCoroutine("IdleForASeconds");

        //Como se quiere que vuelva a caminar, pasara a false isIdle y actualizara la posicion hacia la que se tiene que dirigir.
        isIdle = false;
        SetNewPosition();
        arrivedToPos = false;
    }

    //Actualiza la posicion hacia la que se tiene que dirigir el enemigo.
    void SetNewPosition()
    {
        if (!isFacingRight)
            actualPosition = new Vector2(leftLimit, transform.position.y);
        else
            actualPosition = new Vector2(rightLimit, transform.position.y);
    }

    //Es llamado desde el SoldierPlayerDetect.cs, cuando colisiona con el jugador para que comience la accion de atacar
    public void CanAttack()
    {
        playerDetect.SetActive(false);
        _animator.SetTrigger("Attack");
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlaySound("soldierAttack");
        isAttacking = true;
    }

    //Se llama como evento al final de la animacion de ataque.
    public void SetFalseIsAttacking()
    {
        isAttacking = false;
        StartCoroutine("CooldownAttack");
    }

    //Corutina para que el enemigo no pueda atacar mas durante un turno.
    IEnumerator CooldownAttack()
    {
        yield return new WaitForSeconds(1);

        playerDetect.SetActive(true);

        yield return null;
    }
}
