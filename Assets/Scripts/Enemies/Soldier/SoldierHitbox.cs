using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierHitbox : MonoBehaviour
{
    public int damage = 1;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            collision.SendMessageUpwards("AddDamage", damage);
    }
}
