using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierPlayerDetect : MonoBehaviour
{
    public Soldier soldier;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            soldier.SendMessageUpwards("CanAttack");
            
    }
}
