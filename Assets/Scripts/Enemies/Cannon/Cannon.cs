using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform _firePoint;
    public AudioSource _cannonAudio;
    public bool stop = false;

    Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
        InvokeRepeating("ChangeToShootAnimation", 0f, 3f);
    }

    void ChangeToShootAnimation()
    {
        if(bulletPrefab != null && _firePoint != null)
            _animator.SetTrigger("Shoot");
    }

    public void Shoot()
    {
        if (bulletPrefab != null && _firePoint != null)
        {
            if (!stop)
                _cannonAudio.Play();

            GameObject bullet = Instantiate(bulletPrefab, _firePoint.position, Quaternion.identity) as GameObject;
            Bullet bulletComponent = bullet.GetComponent<Bullet>();

            if (this.transform.localScale.x > 0f)
                bulletComponent.direction = Vector2.left;
            else
                bulletComponent.direction = Vector2.right;
        }
    }
}
