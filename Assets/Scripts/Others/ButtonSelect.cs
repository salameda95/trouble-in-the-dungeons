using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonSelect : MonoBehaviour, ISelectHandler, ISubmitHandler
{
    public bool firstButton;

    AudioManager _audioManager;
    bool firstTime = true;
    

    void Awake()
    {
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    void OnEnable()
    {
        firstTime = true;
    }
    void OnDisable()
    {
        firstTime = true;
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (firstButton && !firstTime)
            _audioManager.PlaySound("selectButton");
        else if (firstButton && firstTime)
            firstTime = false;
        else
            _audioManager.PlaySound("selectButton");
    }

    public void OnSubmit(BaseEventData eventData)
    {
        _audioManager.PlaySound("confirm");
    }
}
