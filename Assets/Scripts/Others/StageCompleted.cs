using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class StageCompleted : MonoBehaviour
{
    public Color initialColorBackground;
    public Color finalColorBackground;
    public float duration;
    public Color initialColorText;
    public Color finalColorText;
    public Text gratitudeText;
    public Text signatureText;
    public PlayerInput _playerInput;
    public AudioSource ambientalAudio;

    float startingTime;
    bool fadeBackgroundCompleted;
    bool fadeTextCompleted;

    void OnEnable()
    {
        ambientalAudio.Stop();
        GameObject.Find("Cannon").GetComponent<Cannon>().stop = true;
        startingTime = Time.time;
        fadeBackgroundCompleted = false;
        fadeTextCompleted = false;
        _playerInput.enabled = false;
    }

    void Update()
    {
        if (!fadeBackgroundCompleted)
        {
            float timeSinceStarted = Time.time - startingTime;
            float percentageCompleted = timeSinceStarted / duration;

            GetComponent<Image>().color = Color.Lerp(initialColorBackground, finalColorBackground, percentageCompleted);

            if (percentageCompleted >= 1f)
            {
                fadeBackgroundCompleted = true;
                startingTime = Time.time;
            }
                
        }else if (!fadeTextCompleted)
        {
            float timeSinceStarted = Time.time - startingTime;
            float percentageCompleted = timeSinceStarted / duration;

            gratitudeText.color = Color.Lerp(initialColorText, finalColorText, percentageCompleted);
            signatureText.color = Color.Lerp(initialColorText, finalColorText, percentageCompleted);

            if (percentageCompleted >= 1f)
            {
                fadeTextCompleted = true;
                StartCoroutine("closeGame");
            }
        }
    }

    IEnumerator closeGame()
    {
        yield return new WaitForSeconds(5f);

        Application.Quit();
    }
}
