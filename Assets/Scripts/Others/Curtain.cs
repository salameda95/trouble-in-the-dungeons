using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Curtain : MonoBehaviour
{
    public Transform mask;
    public Transform playerPosition;
    public PlayerInput _playerInput;
    public float scaleToDisapear;
    public float speed;
    public Dialog dialog;

    void Start()
    {
        mask.position = new Vector2(playerPosition.position.x, playerPosition.position.y);
    }

    void Update()
    {
        mask.localScale = new Vector2(mask.localScale.x + speed, mask.localScale.y + speed);

        if (mask.localScale.x >= scaleToDisapear || mask.localScale.y >= scaleToDisapear)
        {
            _playerInput.enabled = true;
            Destroy(gameObject);

            _playerInput.gameObject.GetComponent<PlayerActions>().ReadTextTutorial(dialog);
        }
    }
}
