using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitStage : MonoBehaviour
{
    public GameObject winStageCanvas;

    AudioManager _audioManager;

    private void Awake()
    {
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            winStageCanvas.SetActive(true);
            _audioManager.PlaySound("winStage");
        }
            
    }
}
