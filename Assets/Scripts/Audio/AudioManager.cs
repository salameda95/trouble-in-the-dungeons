using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip attack, death, jump, landing, walking, hit, openDialog, pause, health, selectButton, confirm, stairDown, winStage, ratDeath, soldierDeath, soldierAttack;

    AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(string clip)
    {
        switch (clip)
        {
            case "attack": _audioSource.PlayOneShot(attack); break;
            case "death": _audioSource.PlayOneShot(death); break;
            case "hit": _audioSource.PlayOneShot(hit); break;
            case "ratDeath": _audioSource.PlayOneShot(ratDeath); break;
            case "soldierDeath": _audioSource.PlayOneShot(soldierDeath); break;
            case "soldierAttack": _audioSource.PlayOneShot(soldierAttack); break;
            case "jump": _audioSource.PlayOneShot(jump); break;
            case "landing": _audioSource.PlayOneShot(landing); break;
            case "walking": _audioSource.PlayOneShot(walking); break;
            case "openDialog": _audioSource.PlayOneShot(openDialog); break;
            case "pause": _audioSource.PlayOneShot(pause); break;
            case "health": _audioSource.PlayOneShot(health); break;
            case "selectButton": _audioSource.PlayOneShot(selectButton); break;
            case "confirm": _audioSource.PlayOneShot(confirm); break;
            case "winStage": _audioSource.PlayOneShot(winStage); break;
            default: _audioSource.PlayOneShot(null); break;
        }
    }

    public void StopSound()
    {
        _audioSource.Stop();
    }
}
