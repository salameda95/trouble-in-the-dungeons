using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuNavigation : MonoBehaviour
{
    public Button playButton;
    public Button exitButton;
    public GameObject controlsPanel;

    public void StartGame()
    {
        EventSystem.current.SetSelectedGameObject(null);
        playButton.interactable = false;
        playButton.GetComponent<Image>().raycastTarget = false;

        exitButton.interactable = false;
        exitButton.GetComponent<Image>().raycastTarget = false;

        controlsPanel.SetActive(true);
        controlsPanel.transform.Find("Keyboard").gameObject.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
