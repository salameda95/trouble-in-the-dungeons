using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadePanel : MonoBehaviour
{
    public float duration = 5f;
    public GameObject panelToEnable;

    void OnEnable()
    {
        StartCoroutine("OpenNextPanel", duration);
    }

    IEnumerator OpenNextPanel(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        if (panelToEnable != null)
        {
            panelToEnable.SetActive(true);
            this.gameObject.SetActive(false);
            StopAllCoroutines();
        }
        else
        {
            SceneManager.LoadScene("TutorialStage");
            StopAllCoroutines();
        }
        
    }
}
